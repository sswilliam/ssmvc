package net.sswilliam.ssmvc.test;


import net.sswilliam.ssmvc.impl.Context;
import net.sswilliam.utils.sleeper.Sleeper;

public class ContextTestUtils {

	public ContextTestUtils() {
		// TODO Auto-generated constructor stub
	}

	public static void waitEntityRegisterd(Context context, @SuppressWarnings("rawtypes") Class clazz, String name, long timeout) throws Exception{
		
		long startTime = System.currentTimeMillis();
		while(true){
			@SuppressWarnings("unchecked")
			Object targetObj = context.getEntity(clazz, name);
			if(targetObj != null){
				return;
			}
			if(System.currentTimeMillis() - startTime > timeout){
				throw new Exception("time out to wait "+clazz+"  not null");
			}
			Sleeper.sleep(50);
		}
	}

	public static void waitEntityRegisterd(Context context, @SuppressWarnings("rawtypes") Class clazz, long timeout) throws Exception{
		
		waitEntityRegisterd(context, clazz,"", 10000);
	}
	public static void waitEntityRegisterd(Context context,@SuppressWarnings("rawtypes") Class clazz, String name) throws Exception{
		waitEntityRegisterd(context, clazz,name,10000);
	}
	public static void waitEntityRegisterd(Context context, @SuppressWarnings("rawtypes") Class clazz) throws Exception{
		waitEntityRegisterd(context, clazz,10000);
		
	
	}
}
