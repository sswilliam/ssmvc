package net.sswilliam.ssmvc.impl;

import java.util.HashMap;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import net.sswilliam.ssmvc.design.IContext;

public abstract class Context implements IContext {

	private HashMap<Class<?>, HashMap<String, Object>> entitys;
	private String name;
	private ExecutorService syncBus;
	private ExecutorService asyncPool;

	public Context(String name) {
		this.name = name;
		init();
		// TODO Auto-generated constructor stub
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return name;
	}

	@Override
	public final void init() {
		// TODO Auto-generated method stub
		this.entitys = new HashMap<Class<?>, HashMap<String, Object>>();
		syncBus = Executors.newSingleThreadExecutor();
		asyncPool = Executors.newCachedThreadPool();
	}

	@Override
	public void recycle() {
		// TODO Auto-generated method stub
		syncBus.shutdown();
		asyncPool.shutdown();
		this.entitys.clear();
	}

	@Override
	public void registerEntity(Class<?> clazz, Object entity) {
		registerEntity(clazz, "", entity);
	}

	@Override
	public void registerEntity(Class<?> clazz, String name, Object entity) {

		HashMap<String, Object> keyValues = entitys.get(clazz);
		if (keyValues == null) {
			keyValues = new HashMap<String, Object>();
			entitys.put(clazz, keyValues);
		}
		keyValues.put(name, entity);
	}

	@Override
	public void unregisterEntity(Class<?> clazz) {
		// TODO Auto-generated method stub

		unregisterEntity(clazz, "");
	}

	@Override
	public void unregisterEntity(Class<?> clazz, String name) {
		// TODO Auto-generated method stub

		HashMap<String, Object> keyValues = entitys.get(clazz);
		if (keyValues == null) {
			return;
		}
		keyValues.remove(name);
		if (keyValues.size() == 0) {
			entitys.remove(clazz);
		}
	}

	@Override
	public <T> T getEntity(Class<T> clazz) {
		// TODO Auto-generated method stub

		return getEntity(clazz, "");
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> T getEntity(Class<T> clazz, String name) {
		HashMap<String, Object> keyValues = entitys.get(clazz);
		if (keyValues == null) {
			return null;
		}
		return (T) keyValues.get(name);
	}

	@Override
	public void postTaskSync(Runnable task) {
		// TODO Auto-generated method stub
		this.syncBus.submit(task);

	}

	@Override
	public void postTaskAsync(Runnable task) {
		// TODO Auto-generated method stub
		this.asyncPool.submit(task);

	}

	@Override
	public Future<?> postTaskSync(Callable<?> task) {
		// TODO Auto-generated method stub
		return syncBus.submit(task);

	}

	@Override
	public Future<?> postTaskAsync(Callable<?> task) {
		// TODO Auto-generated method stub
		return asyncPool.submit(task);

	}

	public ExecutorService getAsyncPool() {
		return this.asyncPool;
	}

}
