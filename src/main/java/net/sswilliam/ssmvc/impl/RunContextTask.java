package net.sswilliam.ssmvc.impl;

import net.sswilliam.ssmvc.design.ITaskMonitor;

public abstract class RunContextTask extends RunTask {

	protected Context context;
	public RunContextTask(ITaskMonitor monitor, Context context) {
		super(monitor);
		this.context = context;
		// TODO Auto-generated constructor stub
	}
	public RunContextTask(Context context){
		this(null,context);
	}

}
