package net.sswilliam.ssmvc.impl;

import net.sswilliam.ssmvc.design.ITaskMonitor;

public abstract class CallContextTask extends CallTask {

	protected Context context;
	public CallContextTask(ITaskMonitor monitor, Context context) {
		super(monitor);
		this.context = context;
	}

	public CallContextTask(Context context){
		this(null, context);
	}
}
