package net.sswilliam.ssmvc.impl;

import net.sswilliam.ssmvc.design.ITaskMonitor;

public class TaskBase implements ITaskMonitor {

	protected ITaskMonitor monitor;
	public TaskBase(ITaskMonitor monitor) {
		// TODO Auto-generated constructor stub
		this.monitor = monitor;
	}

	@Override
	public void begin(String msg) {
		// TODO Auto-generated method stub
		if(monitor != null){
			monitor.begin(msg);
		}
		
	}
	
	@Override
	public void end(String msg) {
		if(monitor != null){
			monitor.end(msg);
		}
	}
	
	@Override
	public void step(int step, String msg) {
		if(monitor != null){
			monitor.step(step, msg);
		}
	}
	
	@Override
	public void error(String msg) {
		if(monitor != null){
			monitor.error(msg);
		}
	}
	
	@Override
	public int getStep() {
		// TODO Auto-generated method stub
		if(monitor != null){
			return monitor.getStep();
		}
		return 0;
	}
}
