package net.sswilliam.ssmvc.impl;

import net.sswilliam.ssmvc.design.ITaskMonitor;
import net.sswilliam.utils.log.CallStackUtils;

public abstract class RunTask extends TaskBase implements Runnable{

	public RunTask(ITaskMonitor monitor) {
		super(monitor);
		// TODO Auto-generated constructor stub
	}
	@Override
	public void run() {
		// TODO Auto-generated method stub
		try {
			doRun();
		} catch (Exception e) {
			e.printStackTrace();
			if(monitor != null){
				monitor.error(CallStackUtils.getCallStack(e));
			}
			// TODO: handle exception
		}
		
	}
	public abstract void doRun() throws Exception;
	
	

	
}
