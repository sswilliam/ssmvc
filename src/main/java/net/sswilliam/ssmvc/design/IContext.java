package net.sswilliam.ssmvc.design;

import java.util.concurrent.Callable;
import java.util.concurrent.Future;

/**
 * 
 * @author sswilliam
 * 
 * SSMVC is a very very light-weight mvc framework which you can see, we only have one interface
 * 
 * 
 * In SSMVC, the context is the heart of the entire application
 * All the components of the application is registerrd in the context and could be retrieved from the contextl
 * in the context we also have one main thread which, task are execuete on it one by one
 * and we also have a thread pool to execute task immedidately .
 * 
 * 
 */
public interface IContext {

	public String getName();
	
	public void start(String[] args);
	public void init();
	public void recycle();
	public void stop();
	
	
	//module and view
	public void registerEntity(Class<?> clazz, Object entity);
	public void registerEntity(Class<?> clazz, String name, Object entity);
	public void unregisterEntity(Class<?> clazz);
	public void unregisterEntity(Class<?> clazz, String name);
	public <T > T getEntity(Class<T>  clazz);
	public <T > T getEntity(Class<T>  clazz, String name);
	
	
	//controller
	public void postTaskSync(Runnable task);
	public void postTaskAsync(Runnable task);
	public Future<?> postTaskSync(Callable<?> task);
	public Future<?> postTaskAsync(Callable<?> task);
}
