package net.sswilliam.ssmvc.design;

public interface ITaskMonitor {

	public void begin(final String msg);
	public void end(final String msg);
	public void step(final int step, final String msg);
	public void error(final String msg);
	public int getStep();
}
