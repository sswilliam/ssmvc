package net.sswilliam.utils.timestamp;

import java.text.SimpleDateFormat;
import java.util.Date;

public class TimeStampUtil {

	public TimeStampUtil() {
		// TODO Auto-generated constructor stub
	}

	public static SimpleDateFormat  format = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss_SSS");
	public static String getCurrentTimeStamp(){
		return getTimeStampeStr(new Date());
	}
	public static String getTimeStampeStr(Date date){
		return format.format(date);
	}
}
