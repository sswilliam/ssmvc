package net.sswilliam.utils;

import java.awt.Component;
import java.awt.Container;
import java.awt.Toolkit;

import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;

public class SwingUtils {

	public static int screen_w;
	public static int screen_h;
	static{
		Toolkit kToolkit = Toolkit.getDefaultToolkit();
		screen_w = kToolkit.getScreenSize().width;
		screen_h = kToolkit.getScreenSize().height;
	}
	public static void makeFrameCenter(JFrame frame){
		if(screen_w == 1200 && screen_h == 1600){
			frame.setLocation((screen_w - frame.getWidth())/2
//					+offsetx
					, (screen_h - frame.getHeight())/2
//					+offsety
					);
//			frame.setLocation(1200 + (2560 - frame.getWidth())/2+offsetx, (1440 - frame.getHeight())/2+offsety);
		}else if(screen_w == 1280 && screen_h == 800){
			frame.setLocation((screen_w - frame.getWidth())/2
//					+offsetx
					, (screen_h - frame.getHeight())/2
//					+offsety
					);
		}else{
			frame.setLocation((screen_w - frame.getWidth())/2
//					+offsetx
					, (screen_h - frame.getHeight())/2
//					+offsety
					);
		}
			
	}
	public static void makeRelatedCenter(Component target, Component self){
		self.setLocation(target.getX()+target.getWidth()/2 - self.getWidth()/2, target.getY()+target.getHeight()/2 - self.getHeight()/2);
	}
	
	public static void showFrame(final JFrame target){
		SwingUtilities.invokeLater(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				target.setVisible(true);
			}
		});
	}
	
	
	
	public static void add(Container parent, JComponent comp) {
		add(parent, comp, comp.getX(), comp.getY(), comp.getWidth(),
				comp.getHeight());
	}

	public static void add(Container parent, JComponent comp, int x, int y, int w,
			int h) {
		comp.setBounds(x, y, w, h);

		// if(comp.getBorder() == null){
		//
		// // comp.setBorder(new LineBorder(Color.RED));
		// }
		// comp.setBorder(new LineBorder(Color.RED));

		parent.add(comp);
	}

	public static void add(Container parent, JComponent comp, int x, int y) {
		add(parent, comp, x, y, comp.getWidth(), comp.getHeight());
	}

	public static void centerHorizontal(Component parent, Component target) {
		target.setLocation((parent.getWidth() - target.getWidth()) / 2,
				target.getY());
	}
	public static void centerVertical(Component parent, Component target){
		target.setLocation(target.getY(),(parent.getHeight() - target.getHeight())/2);
	}
	public static void centerHAndV(Component parent, Component target){
		target.setLocation((parent.getWidth() - target.getWidth()) / 2,
				(parent.getHeight() - target.getHeight())/2);
	}
	public static void moveOffset(JFrame frame, int x, int y){
		frame.setLocation(frame.getX()+x, frame.getY()+y);
	}
	
}
