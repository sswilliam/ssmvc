package net.sswilliam.utils;

import java.lang.reflect.Field;

import net.sswilliam.utils.sleeper.Sleeper;

public class TestUtils {

	public TestUtils() {
		// TODO Auto-generated constructor stub
	}
	public static boolean isDebug = false;
	public static int getDefaultTimeout(){
		if(isDebug){
			return Integer.MAX_VALUE;
		}
		return 10000;
	}
	public static void waitNotNull(Object target, String fieldName, long timeout) throws Exception{
		Field field = target.getClass().getDeclaredField(fieldName);
		field.setAccessible(true);
		long startTime = System.currentTimeMillis();
		while(true){
			Object targetObj = field.get(target);
			if(targetObj != null){
				return;
			}
			if(System.currentTimeMillis() - startTime > timeout){
				throw new Exception("time out to wait "+fieldName+"  not null");
			}
			Sleeper.sleep(50);
		}
	}
	public static void waitNotNull(Object target, String fieldName) throws Exception{
		waitNotNull(target, fieldName, getDefaultTimeout());
	}

}
