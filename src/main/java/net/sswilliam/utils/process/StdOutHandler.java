package net.sswilliam.utils.process;

public class StdOutHandler implements IProcessExecuteOutHandler {

	private IProcessExecuteOutHandler other;
	public StdOutHandler(IProcessExecuteOutHandler other) {
		// TODO Auto-generated constructor stub
		this.other = other;
	}

	@Override
	public void onStdout(String line) {
		// TODO Auto-generated method stub

		System.out.println(line);
		if(other != null){
			other.onStdout(line);
		}
	}

	@Override
	public void onStderr(String line) {
		// TODO Auto-generated method stub
		System.err.println(line);
		if(other != null){
			other.onStderr(line);
		}

	}

}
