package net.sswilliam.utils.process;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

public class SyncProcessRunner2 {

	ProcessBuilder builder;
	IProcessExecuteOutHandler handler;

	public SyncProcessRunner2(List<String> commands, String workingDir,
			IProcessExecuteOutHandler monitor) {
		// TODO Auto-generated constructor stub
		this.builder = new ProcessBuilder(commands);
		this.builder.directory(new File(new File(workingDir).getAbsolutePath()));
		System.out.println("run commands "+commands+"  under "+builder.directory().getAbsolutePath());
		this.handler = monitor;
	}

	public void execute() throws IOException, InterruptedException {
		final Process process = builder.start();
		new Thread(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				try {

					BufferedReader reader = new BufferedReader(
							new InputStreamReader(process.getInputStream()));
					String line;
					while ((line = reader.readLine()) != null) {
						if (handler != null) {

							handler.onStdout(line);
						} else {
							System.out.println("[STD]" + line);
						}
					}
					reader.close();
				} catch (Exception e) {
					e.printStackTrace();
					// TODO: handle exception
				}

			}
		}).start();

		new Thread(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				try {

					BufferedReader reader = new BufferedReader(
							new InputStreamReader(process.getErrorStream()));
					String line;
					while ((line = reader.readLine()) != null) {
						if (handler != null) {

							handler.onStderr(line);
						} else {
							System.out.println("[ERR]" + line);
						}
					}
					reader.close();
				} catch (Exception e) {
					e.printStackTrace();
					// TODO: handle exception
				}

			}
		}).start();
		process.waitFor();

	}
	// public SyncProcessRunner(String[] commands, String wor)

}
