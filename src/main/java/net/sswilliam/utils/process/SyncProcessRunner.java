package net.sswilliam.utils.process;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;


public class SyncProcessRunner {

	public String cmd;
	private IProcessExecuteOutHandler outputHandler;
	public SyncProcessRunner(String cmd){
		this(cmd, null);
		
	}
	
	public SyncProcessRunner(String cmd, IProcessExecuteOutHandler outputHandler){
		this.cmd = cmd;
		this.outputHandler = outputHandler;
		
	}
	
	private void doRun(Process process) throws Exception{
		
		InputStream inputStream = process.getInputStream();
		InputStream errorStream = process.getErrorStream();
		final BufferedReader inputReader = new BufferedReader(new InputStreamReader(inputStream));
		final BufferedReader errorReader = new BufferedReader(new InputStreamReader(errorStream));
		new Thread(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				String line = null;
				while(true){
					try {

						line = inputReader.readLine();
					} catch (Exception e) {
						line = null;
						// TODO: handle exception
					}
					if(line == null){
						break;
					}
					if(outputHandler != null){

						outputHandler.onStdout(line);
					}
					
				}
				
			}
		}).start();
		new Thread(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				String line = null;
				while(true){
					try {
						line = errorReader.readLine();
					} catch (Exception e) {
						line = null;
						// TODO: handle exception
					}
					if(line == null){
						break;
					}	
					if(outputHandler != null){

						outputHandler.onStderr(line);
					}
				}
			}
		}).start();
		process.waitFor();
		
	}
	public void run(){
		try {
			
			Process process = Runtime.getRuntime().exec(cmd);
			doRun(process);
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}
	}
	public void run(File domain){
		try {

		
			Process process = Runtime.getRuntime().exec(cmd,new String[]{},domain);
			doRun(process);
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}
	}

	
}
