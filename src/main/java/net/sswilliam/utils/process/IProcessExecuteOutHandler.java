package net.sswilliam.utils.process;

public interface IProcessExecuteOutHandler {

	public void onStdout(String line);
	public void onStderr(String line);
}
