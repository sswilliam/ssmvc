package net.sswilliam.utils.process;

import net.sswilliam.ssmvc.design.ITaskMonitor;

public class TaskHandler implements IProcessExecuteOutHandler {

	private ITaskMonitor monitor;
	public TaskHandler(ITaskMonitor monitor) {
		this.monitor =  monitor;
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onStdout(String line) {
		// TODO Auto-generated method stub
		if(monitor != null){
			monitor.step(0, "[o]"+line+"\r\n");
		}

	}

	@Override
	public void onStderr(String line) {
		// TODO Auto-generated method stub
		if(monitor != null){
			monitor.step(0, "[e]"+line+"\r\n");
		}
	}

}
