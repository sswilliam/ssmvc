package net.sswilliam.utils.process;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;

public class ProcessLogHandler implements IProcessExecuteOutHandler {

	private IProcessExecuteOutHandler other;
	private BufferedWriter writer;
	public ProcessLogHandler(String path, IProcessExecuteOutHandler other){
		this.other = other;
		File file = new File(path);
		if(file.exists()){
			file.delete();
		}
		try {

			file.createNewFile();
			writer = new BufferedWriter(new FileWriter(file));
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}
		
	}
	
	public void close(){
		try {

			writer.close();
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}
	}
	@Override
	public void onStdout(String line) {
		// TODO Auto-generated method stub
		try {
			writer.write("[I]"+line+"\r\n");
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}
		if(other != null){
			
			other.onStdout(line);
		}

	}

	@Override
	public void onStderr(String line) {
		// TODO Auto-generated method stub
		try {
			writer.write("[E]"+line+"\r\n");
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}
		if(other != null){
			other.onStderr(line);
		}
	}

}
