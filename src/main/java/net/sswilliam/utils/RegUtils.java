package net.sswilliam.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegUtils {

	public static boolean isMatch(String origional, String reg){
		Pattern pattern = Pattern.compile(reg);
		Matcher matcher = pattern.matcher(origional);
		return matcher.find();
	}
}
