package net.sswilliam.utils;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import net.sswilliam.utils.sleeper.Sleeper;


public class SpaceUtils {

	public static long getEFreeSpace() throws Exception{
		String cmd = "C:\\Windows\\System32\\cmd.exe /c dir E:\\";
		System.out.println("cmd");
		Process proc = Runtime.getRuntime().exec(cmd);
		final BufferedReader reader = new BufferedReader(new InputStreamReader(proc.getInputStream()));
		final StringBuffer sb = new StringBuffer();
		new Thread(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				try {
					String line = null;
					while((line = reader.readLine())!= null){
						sb.append(line);
						sb.append("\n");
					}
					reader.close();
				} catch (Exception e) {
					e.printStackTrace();
					// TODO: handle exception
				}
				
			}
		}).start();
		proc.waitFor();
		Sleeper.sleep(100);
		System.out.println(sb.toString());
		String[] tokens = sb.toString().trim().split("\n");
		String lastLine = tokens[tokens.length-1].trim();
		String[] tokens2 = lastLine.split(" ");
		try {

			return Long.parseLong(tokens2[3].replace(",", ""));
		} catch (Exception e) {
			try {

				return Long.parseLong(tokens2[4].replace(",", ""));
			} catch (Exception e2) {
				// TODO: handle exception
				throw e2;
			}
			// TODO: handle exception
		}
	}
	public static void main(String[] args) {
		try {

			System.out.println(getEFreeSpace());
			System.out.println((long)1000*1000*1000);
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}
		 
	}
	private static long minspace = (long)1000*1000*1000;
	public static boolean isSpaceAvailableForDP(){
		try {
			long avaiable = getEFreeSpace();
			if(avaiable > minspace){
				return true;
			}else{
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
			// TODO: handle exception
		}
	}
}
