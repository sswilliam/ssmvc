package net.sswilliam.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class DBUtil {

	
	public static void exec(String sql,String url, String username, String password) throws Exception{
		Connection connection = null;
		Statement statement =  null;
		try {
			connection = DriverManager.getConnection(url, username, password);
			statement = connection.createStatement();
			statement.execute(sql);
			
		} finally{
			if(statement != null){
				statement.close();
			}
			if(connection != null){
				connection.close();
			}
		}
	}
	
	
	public static ResultSet query(String sql,String url, String username, String password) throws Exception{
		
		Connection connection = DriverManager.getConnection(url, username, password);
		
		Statement statement = connection.createStatement();
		
		ResultSet rst = statement.executeQuery(sql);
		return rst;
	}
	public static void releaseRst(ResultSet rst) throws Exception{
		if(rst != null){
			Statement statement = rst.getStatement();
			rst.close();
			statement.getConnection().close();
			statement.close();
		}
	}
	
	public static String filterSQLInjection(String content){
		String canidate = content.toLowerCase().
				replace("delete", "").
				replace("update", "").
				replace("create", "").
				replace("insert", "").
				replace("select", "");
		if(canidate.length() == content.length()){
			return content;
		}else{
			return canidate;
		}
	}
	
	

}
