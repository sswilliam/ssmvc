package net.sswilliam.utils;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class ReflectUtil {
	public static Object getValueByField(Object target,String fieldName){
		try {

			Field field = target.getClass().getDeclaredField(fieldName);
			field.setAccessible(true);
			return field.get(target);
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}
		return null;
	}
	
	
	public static Object call(Object target, String methodName,Class[] types, Object[] args){
		try {
			Method method = target.getClass().getDeclaredMethod(methodName, types);
			
			method.setAccessible(true);
			return method.invoke(target, args);
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}
		return null;
	}

}
