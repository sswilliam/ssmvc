package net.sswilliam.utils.swing;

import java.awt.Component;
import java.awt.Container;
import java.awt.Toolkit;

import javax.swing.JDialog;
import javax.swing.JFrame;

public class SwingKits {
	public static void moveToCenter(JFrame frame,JFrame parent){
		if(frame == null){
			return;
		}
		if(frame == parent){
			return;
		}
		if(parent == null){
			Toolkit kToolkit = Toolkit.getDefaultToolkit();
			int screenH = kToolkit.getScreenSize().height;
			int screenW = kToolkit.getScreenSize().width;
			if(screenW == 1200 && screenH == 1600){
				frame.setLocation(0 - (2560 - frame.getWidth())/2
//						+offsetx
						, (1440 - frame.getHeight())/2
//						+offsety
						);
			}else{
				frame.setLocation((screenW - frame.getWidth())/2
//						+offsetx
						, (screenH - frame.getHeight())/2
//						+offsety
						);
			}
		}else{
			frame.setLocation(parent.getX() + (parent.getWidth() - frame.getWidth())/2, 
					parent.getY() + (parent.getHeight() - frame.getHeight())/2);
		}
		
	}

	
	public static void moveToCenter(JDialog frame,JFrame parent){
		if(frame == null){
			return;
		}
		if(parent == null){
			Toolkit kToolkit = Toolkit.getDefaultToolkit();
			int screenH = kToolkit.getScreenSize().height;
			int screenW = kToolkit.getScreenSize().width;
			if(screenW == 1200 && screenH == 1600){
				frame.setLocation(1200 + (2560 - frame.getWidth())/2
//						+offsetx
						, (1440 - frame.getHeight())/2
//						+offsety
						);
			}else{
				frame.setLocation((screenW - frame.getWidth())/2
//						+offsetx
						, (screenH - frame.getHeight())/2
//						+offsety
						);
			}
		}else{
			frame.setLocation(parent.getX() + (parent.getWidth() - frame.getWidth())/2, 
					parent.getY() + (parent.getHeight() - frame.getHeight())/2);
		}
		
	}
	
	public static void sAddComponent(Container container, Component  comp, int x, int y, int w, int h){
		container.add(comp);
//		if(comp instanceof JComponent){
//			JComponent comp2 = (JComponent)comp;
//			if(comp2.getBorder() == null){
//				comp2.setBorder(new LineBorder(Color.RED));
//			}
//		}
		comp.setBounds(x, y, w, h);
	}
	

	public static void sAddComponent(Container container, Component comp){
		container.add(comp);
	}

}
