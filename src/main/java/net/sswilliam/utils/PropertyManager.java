package net.sswilliam.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

public class PropertyManager {

	Properties properties;
	private String path;
	public PropertyManager(String path) throws Exception{
		// TODO Auto-generated constructor stub
		if(!path.toLowerCase().endsWith(".properties")){
			throw new Exception("the path should end up with a .properties suffix");
		}
		this.path = path;
		File file = new File(path);
		if(!file.exists()){
			file.getAbsoluteFile().getParentFile().mkdirs();
			file.createNewFile();
		}
		properties = new Properties();
		properties.load(new FileInputStream(file));
	}
	
	public void save() throws IOException{
		properties.store(new FileOutputStream(new File(path)), "");
	}
	public String getString(String key){
		String value = properties.getProperty(key);
		if(value == null){
			return "";
		}
		return value;
	}
	public int getInt(String key){
		String strValue  = getString(key);
		return Integer.parseInt(strValue);
	}
	public double getDouble(String key){

		String strValue  = getString(key);
		return Double.parseDouble(strValue);
	}
	public void putString(String key, String value){
		properties.put(key, value);
	}
	public void putInt(String key,int value){
		properties.put(key, value+"");
		
	}
	public void putDouble(String key, double value){
		properties.put(key, value+"");
		
	}
	

}
