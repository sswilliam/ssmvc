package net.sswilliam.utils.log;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

public class CallStackUtils {

	public static String getCallStack(Throwable e){
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		PrintStream outstream = new PrintStream(out);
		e.printStackTrace(outstream);
		try {
			String result = out.toString("UTF-8");

			out.close();
			outstream.close();
			return result;
			
		} catch (Exception e2) {
			return "";
		}
	}
	public static void printCurrentCallStack(){
		try {
			throw new Exception("T_T");
		} catch (Exception e) {
			System.out.println(CallStackUtils.getCallStack(e));
			// TODO: handle exception
		}
	}
}
