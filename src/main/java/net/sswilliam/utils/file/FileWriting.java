package net.sswilliam.utils.file;

import java.io.*;

public class FileWriting {
	private String pathfile;
	private File file;
	private PrintWriter prtWriter;

	public FileWriting(String name) {
		pathfile = name;
	}

	public void initialize() throws IOException {
		file = new File(pathfile);
		if (file.exists())
			file.delete();// delete if it already exists
		file.createNewFile(); // create a new file
		prtWriter = new PrintWriter(new BufferedWriter(new FileWriter(file, true)));
	}

	public void initialize_UTF8() throws IOException {
		file = new File(pathfile);
		// if(file.exists()) file.delete();//delete if it already exists
		// file.createNewFile(); //create a new file
		// prtWriter=new PrintWriter(file,"UTF-8");

		// #796747, possible root cause: run multiple JavaERT instances at the same time
		synchronized (FileWriting.class) {
			if (file.exists()) {
				if (!file.delete()) {
					throw new IOException("Cannot delete existing file: " + pathfile);
				}
			}
			if (!file.createNewFile()) {
				throw new IOException("Cannot create new file: " + pathfile);
			}
			prtWriter = new PrintWriter(file, "UTF-8");
		}
	}

	public void initialize_UTF16() throws IOException {
		file = new File(pathfile);
		if (file.exists())
			file.delete();// delete if it already exists
		file.createNewFile(); // create a new file
		prtWriter = new PrintWriter(file, "UTF-16");
	}

	public void appendContent(String content) {
		if (prtWriter == null)
			return;
		prtWriter.println(content.replaceAll("\n", System.getProperty("line.separator")));
		prtWriter.flush();
	}

	public void close() {
		if (prtWriter == null)
			return;
		prtWriter.close();
	}

	public void finalized() {
		file = null;
		prtWriter = null;
	}

}
